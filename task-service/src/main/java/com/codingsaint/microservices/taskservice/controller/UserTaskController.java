package com.codingsaint.microservices.taskservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codingsaint.microservices.taskservice.model.Task;
import com.codingsaint.microservices.taskservice.model.Tasks;
import com.codingsaint.microservices.taskservice.model.UserTask;
import com.codingsaint.microservices.taskservice.repository.TaskRepository;
import com.codingsaint.microservices.taskservice.repository.UserTaskRepository;

@RestController
public class UserTaskController {
	Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserTaskRepository userTaskRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@PostMapping("v1/user/task")
	public ResponseEntity<Void> saveUserTask(@RequestBody UserTask userTask){
		userTaskRepository.save(userTask);
		HttpHeaders headers=new HttpHeaders();
		headers.add("id", userTask.getId().toString());		
		ResponseEntity<Void> response= new ResponseEntity<Void>(headers,HttpStatus.CREATED);
		return response;
		
	}
	@GetMapping("v1/user/{userId}/tasks")
	public ResponseEntity<Tasks> getUserTasks(@PathVariable Integer userId){
		logger.info("{}",userId);
		List<UserTask>  userTasks=userTaskRepository.getByUserId(userId);
		List<Task> taskList= new ArrayList<Task>();
		for(UserTask userTask:userTasks){
			logger.info("task id {}",userTask.getTask().getId());
			Task task=taskRepository.getOne(userTask.getTask().getId());
			taskList.add(task);
		}
		Tasks tasks= new Tasks();
		tasks.setTasks(taskList);
		HttpHeaders headers=new HttpHeaders();
		ResponseEntity<Tasks> response= new ResponseEntity<Tasks>(tasks,headers,HttpStatus.OK);
		return response;
		
	}

}
