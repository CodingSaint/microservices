package com.codingsaint.microservices.taskservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.codingsaint.microservices.taskservice.model.Task;

public interface TaskRepository extends JpaRepository<Task, Integer>{

}
