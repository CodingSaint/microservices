package com.codingsaint.microservices.taskservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codingsaint.microservices.taskservice.model.Task;
import com.codingsaint.microservices.taskservice.repository.TaskRepository;

@RestController
public class TaskController {

	@Autowired
	private TaskRepository taskRepository;
	
	@GetMapping("/v1/tasks")
	public List<Task> getTasks(){
		return taskRepository.findAll();
	}
	@GetMapping("/v1/tasks/{id}")
	public Optional<Task> getTask(@PathVariable ("id") Integer id){
		return taskRepository.findById(id);
	}
	
	@PostMapping("/v1/task")
	public ResponseEntity<Void> getTask(@RequestBody Task task){
		HttpHeaders headers=new HttpHeaders();		
		taskRepository.save(task);
		headers.add("id", task.getId().toString());
		ResponseEntity<Void> response= new ResponseEntity<>(headers,HttpStatus.CREATED);
		return response;
	}
	
	
}
