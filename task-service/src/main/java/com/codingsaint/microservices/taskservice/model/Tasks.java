package com.codingsaint.microservices.taskservice.model;

import java.util.List;

public class Tasks {
	private List<Task> tasks;

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	

}
