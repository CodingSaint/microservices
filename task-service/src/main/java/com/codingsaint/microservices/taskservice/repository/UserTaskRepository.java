package com.codingsaint.microservices.taskservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.codingsaint.microservices.taskservice.model.UserTask;

public interface UserTaskRepository extends JpaRepository<UserTask, Integer>{
	List<UserTask> getByUserId(@Param ("userId") Integer userId);

}
