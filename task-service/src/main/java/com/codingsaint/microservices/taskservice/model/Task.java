package com.codingsaint.microservices.taskservice.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "task")
public class Task {
	public Task(){}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;	
	@Column(name="task_name")
	private String taskName;
	
	@Column(name="task_desc")
	private String taskDesc;
	
	/*@ManyToMany(cascade = CascadeType.ALL) 
	@JoinTable(name = "user_task", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "task_id"))
	private List<UserTask> userTask;*/
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskDesc() {
		return taskDesc;
	}

	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	/*public List<UserTask> getUserTask() {
		return userTask;
	}

	public void setUserTask(List<UserTask> userTask) {
		this.userTask = userTask;
	}*/


	
	
	
}
