package com.codingsaint.microservices.userservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.codingsaint.microservices.userservice.model.Tasks;


@FeignClient(name="task-service" ,url="http://localhost:8090")

public interface TasksClientProxyService {	
	@GetMapping("v1/user/{userId}/tasks")
	ResponseEntity<Tasks> getUserTasks(@PathVariable("userId") Integer userId);
}
