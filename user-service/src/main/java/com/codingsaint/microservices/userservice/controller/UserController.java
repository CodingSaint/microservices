package com.codingsaint.microservices.userservice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.codingsaint.microservices.userservice.model.Tasks;
import com.codingsaint.microservices.userservice.model.User;
import com.codingsaint.microservices.userservice.repository.UserRepository;
import com.codingsaint.microservices.userservice.service.TasksClientProxyService;

@RestController
public class UserController {
	@Autowired
	UserRepository userRepository;
	@Autowired
	private Environment env;
	@Autowired
	private TasksClientProxyService proxy;
	Logger logger=LoggerFactory.getLogger(this.getClass());
	
	
	@GetMapping("/v1/users")
	public List<User> get(){
		logger.info("Inside get->UserController");
		return userRepository.findAll();
		
	}
	@GetMapping("/v1/hi")
	public String hi(){
		return env.getProperty("welcome.message");
		
	}
	@GetMapping("/v1/users/{id}")
	public Optional<User> get(@PathVariable ("id") Integer id){
		return userRepository.findById(id);
		
	}
	 
	@PostMapping("/v1/user")
	public ResponseEntity<Void> save(@RequestBody User user){
		HttpHeaders headers=new HttpHeaders();		
		userRepository.save(user);
		headers.add("id", user.getId().toString());
		ResponseEntity<Void> response= new ResponseEntity<>(headers,HttpStatus.CREATED);
		return response;

	}
	@GetMapping("v1/user/{userId}/tasks")
	public ResponseEntity<Tasks> getUserTasks(@PathVariable Integer userId){
		Map<String,Object> uriVariables=new HashMap<>();
		uriVariables.put("userId", userId);
		ResponseEntity<Tasks> tasks=new RestTemplate().getForEntity("http://localhost:8090/v1/user/{userId}/tasks", Tasks.class, uriVariables);
		return tasks;
	}
	
	@GetMapping("v1/feign/user/{userId}/tasks")
	public ResponseEntity<Tasks> getFeignUserTasks(@PathVariable Integer userId){
		ResponseEntity<Tasks> tasks=proxy.getUserTasks(userId);
		return tasks;
	}

}
