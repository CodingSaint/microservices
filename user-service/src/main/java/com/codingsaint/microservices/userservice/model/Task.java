package com.codingsaint.microservices.userservice.model;

public class Task {
	public Task(){}
	
	private Integer id;	
	
	private String taskName;
	
	
	private String taskDesc;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskDesc() {
		return taskDesc;
	}

	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	

	
	
	
}
