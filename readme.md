
# Welcome to Spring Microserivices : Proof of Concept - Task Microservices !

Hi! This is simple Spring Boot/Spring cloud **Task Microservices**. If you want to learn about Microservices and its implementation via **Spring Boot, Spring Cloud** , you can look at this code base.

# Modules

Though there are several approach to microservices , With Spring Boot standards we can leverage standard component easily using its already provided components .Below are core component we will be using

- **Microservices :** user-service and todo-services

- **Config Server:** All Microservices on startup will refer to config server for their configurations.

- **Api Gateway:** A centralized way to access all the microservices , A front facing unit for all the clients. Also responsible for common services via Filters viz. Authentication , capturing user info in log before hitting services and many other.

- **Feign Client** Helps to remove boilerplate resttTemplate code

- **Ribbon** Along with feign client helps to load balance the application

- **Naming Server: Eureka:** All of the microservices on startup will let naming server know about their existence. Naming server will know their address /location/ ip . Whenever one microservices calls another via ribbon/feign , it will check naming server for existence on another service and return correct one. Because there can be multiple instances of any microservice it helps as in load balancing .

- **Spring Cloud Sleuth** As the name suggests ,sleuth is a detective, To every incoming request it adds tracing mechanism , i.e. traceid . With all logs printing this trace id. We can track over which microservice instance request has traveled.

- **Rabbit MQ** Just to keep track of all traces coming of incoming request. It keeps all these traces at one centralized place.

- **Zipkin** UI for looking tracing the entire journey of request.

- **Hystrix** A fallback mechanism for any failing services.

## Architecture
![enter image description here](https://lh3.googleusercontent.com/8OWwIMWUWCGy3KJR4H_v5AxG9L6_S1yR9Rwcw3jzX70qhKdvyA0w78j1AupB-MK9kSzAQKVzY7sgahvTBxbavqBy1iNKkZKmqeos8jy5Whc3b4X-rZcsgTpJ5QEw15dNWSsXxkfbBcAzkAeFgf8KfQJTGUROowSri4WJ2Hah3yKwZWic4YVWugl6EDol0TGSTvV8wp97PwzwKac90m2lnUJFoNcZyHSJExXPL0eemOqAkay1wVVySehaIegYk-1a9pPpxY6suFIoxfRNRXe9fRrG79TL6GMq4TiwkxaYKSQy-GhFOpx3eZjVJ8FFRUfVg3o6YlPbMsMuKEJNTs1PlvVnmwuoaGGVhCf3jxPgNCeSuU0Thr2FToMCtkG4Y-ViVch13jZ-5WXed0AztFObjOcVp5puf7e0-psVpW0s1UypjiBUxGNHpjmfgy3To9jf-sMipb3pzl4sWWLP6rAXhDjEgdvfsZlXvweIsqSGyIPSQuOnBmCmuYSchDIbFrJW_5750F-5tXiiKjorHhkjgz0qOzDm9rQCGuXgy6BflAl7mSMwtYp8sdGKfhTKjEY1D1yjTfZcCDTxY_tyIuvsAlRA_DufOMq78LiuO1g=w735-h613-no)
